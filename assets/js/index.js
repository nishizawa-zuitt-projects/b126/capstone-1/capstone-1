const navSlide = () => {
	const menubutton = document.querySelector('.menubutton')
	const navbar = document.querySelector('.navbar')
	const navLinks = document.querySelectorAll('.nav-links li')
	const closebutton = document.querySelector('.closebutton')

	menubutton.addEventListener('click', () =>{
		navbar.classList.toggle('open')

		navLinks.forEach((link, index)=>{
			if(link.style.animation){
				link.style.animation =''
			}else{
				link.style.animation =`navLinkFade 0.5s ease forwards ${index / 5}s`
			}
		})
	})

	closebutton.addEventListener('click', () =>{
		navbar.classList.toggle('open')

		navLinks.forEach((link, index)=>{
			if(link.style.animation){
				link.style.animation =''
			}else{
				link.style.animation =`navLinkFade 0.5s ease forwards ${index / 5}s`
			}
		})
	})
}
navSlide()

const audio = () =>{

	const audio = document.getElementsByTagName("audio")[0]
	const playButton = document.getElementById("play")
	const stopButton = document.getElementById("stop")
	playButton.addEventListener('click', () => {
	  audio.play()
	})
	stopButton.addEventListener('click', () => {
	  audio.pause()
	})

}
audio()